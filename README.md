# ng-describe-dom
> Testing Angular 1 code in synthetic browser using jsdom and ng-describe

[![NPM][ng-describe-jsdom-icon] ][ng-describe-jsdom-url]

[![Build status][ng-describe-jsdom-ci-image] ][ng-describe-jsdom-ci-url]
[![semantic-release][semantic-image] ][semantic-url]

This is a tests-only project that shows how to use synthetic browser
implementation (built on top of [jsdom](https://www.npmjs.com/package/jsdom))
to unit test AngularJS code using [ng-describe](https://www.npmjs.com/package/ng-describe).

The work is still unfinished, mainly due to problems loading both angular and angular-mocks
into the synthetic browser cleanly.

### Small print

Author: Gleb Bahmutov &copy; 2016

* [@bahmutov](https://twitter.com/bahmutov)
* [glebbahmutov.com](http://glebbahmutov.com)
* [blog](http://glebbahmutov.com/blog/)

License: MIT - do anything with the code, but don't blame me if it does not work.

Spread the word: tweet, star on github, etc.

Support: if you find any problems with this module, email / tweet /
[open issue](https://gitlab.com/bahmutov/ng-describe-jsdom/issues) on Github

## MIT License

Copyright (c) 2016 Gleb Bahmutov

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

[ng-describe-jsdom-icon]: https://nodei.co/npm/ng-describe-jsdom.png?downloads=true
[ng-describe-jsdom-url]: https://npmjs.org/package/ng-describe-jsdom
[ng-describe-jsdom-ci-image]: https://gitlab.com/ci/projects/bahmutov%2Fng-describe-jsdom/status.png?ref=master
[ng-describe-jsdom-ci-url]: https://gitlab.com/bahmutov/ng-describe-jsdom
[semantic-image]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
[semantic-url]: https://github.com/semantic-release/semantic-release
