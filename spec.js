const angularPath = './node_modules/angular/angular.js'
// const angularMocksPath = './node_modules/angular-mocks/angular-mocks.js'
const ngDescribePath = './node_modules/ng-describe/dist/ng-describe.js'
const la = require('lazy-ass')
const is = require('check-more-types')
const benv = require('benv')

/* global describe, it, beforeEach, afterEach */
describe('json parse', function () {
  it('fails to stringify undefined', function () {
    var a
    var result = JSON.stringify(a)
    la(typeof result === 'undefined', 'returns undefined')
  })
})

describe('loading ngDescribe in synthetic browser', function () {
  var ngDescribe

  beforeEach(function setupEnvironment (done) {
    benv.setup(function () {
      console.assert(typeof window === 'object', 'has window')

      benv.expose({
        angular: benv.require(angularPath, 'angular'),
        ngDescribe: benv.require(ngDescribePath, 'ngDescribe')
      })
      ngDescribe = window.ngDescribe
      done()
    })
  })

  it('passes a dummy test', function () {})

  it('has window', function () {
    la(typeof window === 'object')
  })

  it('has ngDescribe function', function () {
    la(is.fn(ngDescribe), 'has ngDescribe function')
  })

  it('can have describe inside it', () => {
    describe('inner suite', () => {
      it('nested test', () => {})
    })
  })

  afterEach(function destroySyntheticBrowser () {
    benv.teardown(true)
  })
})

// describe('a variable', function () {
//   beforeEach(function (done) {
//     benv.setup(function () {
//       benv.expose({
//         angular: require(angularPath)
//       })
//       la(window.angular, 'cannot find window.angular')

//       window.mocha = true
//       window.beforeEach = global.beforeEach.bind(global)
//       window.afterEach = global.afterEach.bind(global)

//       benv.require(angularMocksPath)
//       la(window.angular.mock, 'cannot find angular.mock')
//       la(is.fn(window.angular.mock.module),
//         'angular.mock.module not a function', window.angular.mock.module)

//       // benv.expose({
//       //   ngDescribe: benv.require(ngDescribePath)
//       // })
//       console.log('done')
//       done()
//     })
//   })

//   beforeEach(function () {
//     console.log('before each ours, context', typeof this)
//     window.angular.module('A', []).value('foo', 42)
//   })

//   it('dummy', () => {})

//   afterEach(function () {
//     console.log('our after each, context', typeof this)
//   })

//   // it('runs tests', () => {
//   //   window.ngDescribe({
//   //     root: global,
//   //     name: 'module A',
//   //     module: 'A',
//   //     inject: 'foo',
//   //     tests: function (deps) {
//   //       it('has value of foo', () => {
//   //         la(deps.foo === 42)
//   //       })
//   //     }
//   //   })
//   // })

//   afterEach(() => benv.teardown(true))
// })
